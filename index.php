<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity 2</title>
</head>
<body>

    <h1>Divisible of Five</h1>
    <p><?= printDivisibleOfFive(); ?></p>

    <h1>Array Manipulation</h1>
    <p><?php array_push($students, 'John Smith')?></p>
    <p><?php var_dump($students); ?></p>
    <p><?= count($students); ?></p>
    
    <p><?php array_push($students, 'Jane Smith')?></p>
    <p><?php var_dump($students); ?></p>
    <p><?= count($students); ?></p>

    <p><?php array_shift($students)?></p>
    <p><?php var_dump($students); ?></p>
    <p><?= count($students); ?></p>
    
</body>
</html>